from django.db import models

# Create your models here.
from datetime import datetime
from django.utils import timezone

from DjangoUeditor.models import UEditorField   #富文本编辑器


class Mynote(models.Model):
    title = models.CharField(max_length=50, verbose_name=' 技术标题')
    description = UEditorField(u'内容',  # 富文本编辑器，后台中的别名
                               default='',  # 内容为空
                               width=1000,
                               height=300,
                               imagePath='news/images/',  # 上传图像的存储位置
                               filePath='news/files/')  # 上传文件的存储位置

    publishDate = models.DateTimeField(max_length=20,
                                       default=timezone.now,
                                       verbose_name='提交时间', null=True)
    views = models.PositiveIntegerField('浏览量', default=0)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = '生活随笔'
        verbose_name_plural = '生活随笔'