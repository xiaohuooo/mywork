import requests

api_key = "SwRZVzzsr56_nAvB7"
location = "Beijing"
api_url = f"https://api.seniverse.com/v3/weather/now.json?key={api_key}&location={location}"

response = requests.get(api_url)
data = response.json()

if response.status_code == 200:
    result = data["results"][0]
    location = result["location"]
    now = result["now"]

    print("城市：", location["name"])
    print("国家：", location["country"])
    print("天气现象：", now["text"])
    print("温度：", now["temperature"], "℃")
    print("体感温度：", now["feels_like"], "℃")
    print("气压：", now["pressure"], "mb")
    print("风向：", now["wind_direction"])
    print("能见度：", now["visibility"], "km")
    print("湿度：", now["humidity"], "%")
else:
    print("请求失败")