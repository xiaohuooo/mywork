
from django.shortcuts import render
from .tests import chat_with_gpt
from .forms import ChatForm

def chat(request):
    if request.method == "POST":
        print(request.POST)
        form = ChatForm(request.POST)
        print(form)
        if form.is_valid():
            input_text = form.cleaned_data['input_text']

            # 调用ChatGPT进行对话
            response = chat_with_gpt(input_text)
            # 渲染网页模板并返回HTML页面
            return render(request, "chat.html", {"form": form, "response": response})
    else:
        form = ChatForm()

    return render(request, "chat.html", {"form": form})