from django.apps import AppConfig


class SuibiConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "suibi"
