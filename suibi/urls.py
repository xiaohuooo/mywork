from django.urls import path
from . import views

app_name='suibi'

urlpatterns=[
    path('note/', views.note, name='note'),
    path('noteDetail/<int:id>/', views.noteDetail, name='noteDetail'),
    path('search/',views.search,name='search')
]