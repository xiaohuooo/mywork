from django.shortcuts import render, get_object_or_404
from .models import Myjishu  #新闻表
from django.core.paginator import Paginator
from pyquery import PyQuery as pq

def jishu(request,jishuName):
    submenu=jishuName  #侧边导航栏的激活状态
    if jishuName=='Python':
        jishuName='Python'
    elif jishuName=='大数据':
        jishuName='大数据'
    elif jishuName=='前端':
        jishuName='前端'
    else:
        jishuName='NoSql'

    jishuList = Myjishu.objects.all().filter(
        jishuType=jishuName).order_by('-publishDate')
    for myjishu in jishuList:
        html = pq(myjishu.description)  # 使用pq方法解析html内容
        myjishu.mytxt = pq(html)('p').text()  # 截取html段落文字

    p = Paginator(jishuList, 5)  # 过滤好的数据，每页显示5条
    if p.num_pages <= 1:
        pageData = ''  # 向前端传递空串
    else:
        page = int(request.GET.get('page', 1))  # 前端页码超链接 传过来的具体页码
        jishuList = p.page(page)  # 具体页码要显示的5条记录
        left = []  # 列表
        right = []
        left_has_more = False
        right_has_more = False
        first = False
        last = False
        total_pages = p.num_pages  # 总页数
        page_range = p.page_range  # 页数范围
        if page == 1:
            right = page_range[page:page + 2]  # 列表 right[0]=2 right[1]=3
            print(total_pages)
            if right[-1] < total_pages - 1:  # right[-1]为3
                right_has_more = True
            if right[-1] < total_pages:
                last = True
        elif page == total_pages:
            left = page_range[(page - 3) if (page - 3) > 0 else 0:page - 1]  # 倒数第3页，倒数第2页
            if left[0] > 2:  # 倒数第3页页码大于2
                left_has_more = True
            if left[0] > 1:
                first = True
        else:  # 不是第一页或最后一页
            left = page_range[(page - 3) if (page - 3) > 0 else 0:page - 1]
            right = page_range[page:page + 2]
            if left[0] > 2:
                left_has_more = True
            if left[0] > 1:
                first = True
            if right[-1] < total_pages - 1:
                right_has_more = True
            if right[-1] < total_pages:
                last = True
        pageData = {  # 字典，会传给前端
            'left': left,
            'right': right,
            'left_has_more': left_has_more,
            'right_has_more': right_has_more,
            'first': first,
            'last': last,
            'total_pages': total_pages,
            'page': page,
        }

    return render(request,'jishuList.html',{
        'active_menu':'jishu', #base.html新闻动态所在li的id号为jishu
        'sub_menu':submenu,
        'jishuName':jishuName,
        'jishuList':jishuList,
        'pageData': pageData,})

def jishuDetail(request, id):
    myjishu = get_object_or_404(Myjishu, id=id)  # 从MyNew表中获取指定id号的新闻
    myjishu.views += 1
    myjishu.save()
    return render(request, 'jishuDetail.html', {
        'myjishu': myjishu,  # 传递给前端的模板变量
    })

def search(request):
    keyword=request.GET.get('keyword')
    jishuList=Myjishu.objects.filter(title__icontains=keyword) #标题中包含指定关键字
    jishuName="关于"+"\""+keyword+"\""+"的搜索结果"
    return render(request,'searchList.html',{
        'active_menu':'news',
        'jishuName':jishuName,
        'jishuList':jishuList,
    })
