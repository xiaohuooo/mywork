from django import forms

class ChatForm(forms.Form):
    input_text = forms.CharField(
        label='输入对话',
        widget=forms.Textarea(attrs={'rows': 3, 'cols': 50, 'style': 'white-space: pre;'}),
    )
