from django.contrib import admin

# Register your models here.
from .models import Myjishu


class MyjishuAdmin(admin.ModelAdmin):
    style_fields={'description':'ueditor'}

admin.site.register(Myjishu, MyjishuAdmin)
