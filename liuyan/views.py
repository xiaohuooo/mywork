from django.shortcuts import render
from .forms import ResumeForm
# Create your views here.
def recruit(request):
    if request.method == 'POST':
        resumeForm = ResumeForm(data=request.POST, files=request.FILES) #创建模型表单对象
        if resumeForm.is_valid():
            resumeForm.save() #存到数据库
            return render(request, 'success.html', {  #提交了简历，进入success页面
                'active_menu': 'contactus',
                'sub_menu': 'recruit',
            })
    else:  #没有提交
        resumeForm = ResumeForm()  #创建模型表单对象，并传给recruit.html
    return render(
        request, 'liuyan.html', {
            'resumeForm': resumeForm,
        })