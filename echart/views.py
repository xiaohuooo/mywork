from django.shortcuts import render
from .forms import ResumeForm
# Create your views here.
def recruit(request):
    if request.method == 'POST':
        resumeForm = ResumeForm(data=request.POST, files=request.FILES) #创建模型表单对象
        if resumeForm.is_valid():
            resumeForm.save() #存到数据库
            return render(request, 'success.html', {  #提交了简历，进入success页面
                'active_menu': 'contactus',
                'sub_menu': 'recruit',
            })
    else:  #没有提交
        resumeForm = ResumeForm()  #创建模型表单对象，并传给recruit.html
    return render(
        request, 'echart.html', {
            'resumeForm': resumeForm,
        })

from django.shortcuts import HttpResponse
from .models import population
import json

def echart(request):
    # 获取当前登录的用户对象
    return render(request, 'echart.html')
def db_handle(request):
    # 添加数据
    # post = list(population.objects.values('country_name'))
    post = list(population.objects.values()[:5])
    data = json.dumps(post)
    return HttpResponse(data)


from django.db.models import Sum, Max
from django.http import JsonResponse


def echarts6(request):
    start_year = request.GET.get('start_year')
    end_year = request.GET.get('end_year')
    # 计算每个城市的人口占总人口的比例
    city_populations = population.objects.filter(Year__range=(start_year, end_year)).values('name').annotate(
        total_population=Sum('Population')).order_by('-total_population')[:6]
    total_population = \
    population.objects.filter(Year__range=(start_year, end_year)).aggregate(total_population=Sum('Population'))[
        'total_population']

    result_list = []

    for city_population in city_populations:
        city_name = city_population['name']
        pop = city_population['total_population']
        population_percentage = (pop / total_population) * 100 if total_population else 0
        result_list.append({'name': city_name, 'percentage': population_percentage})
    return HttpResponse(json.dumps(list(result_list)), content_type='application/json')


def echarts2(request):
    start_year = request.GET.get('start_year')
    end_year = request.GET.get('end_year')
    total_population = population.objects.filter(Year__range=(start_year, end_year)).values('name').annotate(
        total_population=Sum('Population')).order_by('-total_population')[:5]

    return HttpResponse(json.dumps(list(total_population)), content_type='application/json')


def countAll(request):
    type = request.GET.get('type')
    year = request.GET.get('year')
    country = request.GET.get('country')
    total_population = population.objects.filter(Year=year, country_name=country).values('Year').annotate(
        total_population=Sum(type))

    return HttpResponse(json.dumps(list(total_population)[0]), content_type='application/json')


def echarts1(request):
    type = request.GET.get('type')
    start_year = request.GET.get('start_year')
    end_year = request.GET.get('end_year')
    country = request.GET.get('country')
    total_population = population.objects.filter(Year__range=(start_year, end_year), country_name=country).values(
        'Year').annotate(
        total_population=Sum(type))

    return HttpResponse(json.dumps(list(total_population)))


def echarts4(request):
    start_year = request.GET.get('start_year')
    end_year = request.GET.get('end_year')
    total_population = population.objects.filter(Year__range=(start_year, end_year)).values('Year').annotate(
        total_population=Sum('Population_aged_15_to_64_years'))

    return HttpResponse(json.dumps(list(total_population)))


def echarts5(request):
    start_year = request.GET.get('start_year')
    end_year = request.GET.get('end_year')
    total_population = population.objects.filter(Year__range=(start_year, end_year)).values('Year').annotate(
        total_population=Sum('Population_aged_15_to_64_years'))

    return HttpResponse(json.dumps(list(total_population)))


def echarts31(request):
    start_year = request.GET.get('start_year')
    end_year = request.GET.get('end_year')

    result = {}
    years = population.objects.filter(Year__range=(start_year, end_year)).values('Year').distinct()

    for year in years:
        year = year['Year']
        top_cities = population.objects.filter(Year=year,name__in=["亚洲(联合国)", "非洲", "北美(联合国)", "欧洲(联合国)", "大洋洲(联合国)"]).values('name').annotate(
            value=Max('Population')).order_by('-value')
        result[year] = list(top_cities)

    return JsonResponse(result)


def echarts4(request):
    year = request.GET.get('year')
    country = request.GET.get('country')
    # type = request.GET.get('type')
    type_mapping = {
        '0': 'Population_of_children_under_the_age_of_1',
        '1': 'Population_under_the_age_of_25',
        '2': 'Population_aged_15_to_64_years',
        '3': 'Population'
    }
    # sumtype = type_mapping.get(type, '')
    total_population = population.objects.filter(Year=year, country_name=country).values('name').annotate(
        population=Sum('Population'),
        Population_aged_15_to_64_years=Sum('Population_aged_15_to_64_years'),
        Population_under_the_age_of_25=Sum('Population_under_the_age_of_25'),
        Population_of_children_under_the_age_of_1=Sum('Population_of_children_under_the_age_of_1'),
        Population_aged_90_to_99_years=Sum('Population_aged_90_to_99_years'),
        Population_older_than_100_years=Sum('Population_older_than_100_years')
    )

    return HttpResponse(json.dumps(list(total_population)[0]), content_type='application/json')


def map(request):
    start_year = 1950
    end_year = 2021
    type = request.GET.get('type')
    country = request.GET.get('country')
    type_mapping = {
        '0': 'Population_of_children_under_the_age_of_1',
        '1': 'Population_under_the_age_of_25',
        '2': 'Population_aged_15_to_64_years',
        '3': 'Population'
    }
    sumtype = type_mapping.get(type, '')
    # 查询指定年份范围内的总人口数据
    total_population_data = population.objects.filter(Year__range=(start_year, end_year),country_name=country).values('Year').annotate(
        total_population=Sum(sumtype))

    # 对每年的人口数据进行预估
    # for data in total_population_data:
    #     specified_year = data['Year']
    #
    #     # 获取前五年的人口数据
    #     previous_years_population = population.objects.filter(Year__gte=(specified_year - 5), Year__lt=specified_year)
    #     # 计算人口增长率（假设简单地使用平均增长率）
    #     total_population = sum(getattr(p, sumtype) for p in previous_years_population)
    #     average_growth_rate = total_population / len(previous_years_population)
    #
    #     # 预测指定年份的人口数量
    #     predicted_population = total_population + (
    #                 average_growth_rate * (specified_year - previous_years_population.last().Year))
    #
    #     # 将预估人口数字段添加到数据中
    #     data['predicted_population'] = predicted_population

    return HttpResponse(json.dumps(list(total_population_data)))


def countryList(request):
    unique_names = population.objects.values_list('country_name', flat=True).distinct()
    return HttpResponse(json.dumps(list(unique_names)), content_type='application/json')
