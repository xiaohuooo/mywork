from django.contrib import admin
from django.urls import path
from echart.views import echart,db_handle,echarts6,echarts1,echarts2,echarts31,echarts5,echarts4,map,countryList,countAll

app_name='echart'
urlpatterns = [
    path('', echart, name='echart'),
    path('blogs/', db_handle, name='blogs'),
    path('echarts6/', echarts6, name='echarts6'),
    path('echarts1/', echarts1, name='echarts1'),
    path('echarts2/', echarts2, name='echarts2'),
    path('echarts31/', echarts31, name='echarts31'),
    path('echarts5/', echarts5, name='echarts5'),
    path('echarts4/', echarts4, name='echarts4'),
    path('map/', map, name='map'),
    path('countryList/', countryList, name='countryList'),
    path('countAll/', countAll, name='countAll')
]
