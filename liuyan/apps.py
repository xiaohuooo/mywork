from django.apps import AppConfig


class LiuyanConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "liuyan"
