from django.urls import path
from . import views
app_name='ziliao'
urlpatterns=[
    path('download/',views.download,name='download'),
    path('getDoc/<int:id>/', views.getDoc, name='getDoc'),
    path('search/',views.search,name='search'),

]