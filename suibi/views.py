from django.shortcuts import render, get_object_or_404
from .models import Mynote  #新闻表
from django.core.paginator import Paginator
from pyquery import PyQuery as pq

def note(request):
    submenu = 'note'
    noteList = Mynote.objects.all().order_by('-publishDate')
    p = Paginator(noteList, 5)
    if p.num_pages <= 1:
        pageData = ''
    else:
        page = int(request.GET.get('page', 1))
        noteList = p.page(page)  #那一页的数据
        left = []
        right = []
        left_has_more = False
        right_has_more = False
        first = False
        last = False
        total_pages = p.num_pages
        page_range = p.page_range
        if page == 1:
            right = page_range[page:page + 2]
            print(total_pages)
            if right[-1] < total_pages - 1:
                right_has_more = True
            if right[-1] < total_pages:
                last = True
        elif page == total_pages:
            left = page_range[(page - 3) if (page - 3) > 0 else 0:page - 1]
            if left[0] > 2:
                left_has_more = True
            if left[0] > 1:
                first = True
        else:
            left = page_range[(page - 3) if (page - 3) > 0 else 0:page - 1]
            right = page_range[page:page + 2]
            if left[0] > 2:
                left_has_more = True
            if left[0] > 1:
                first = True
            if right[-1] < total_pages - 1:
                right_has_more = True
            if right[-1] < total_pages:
                last = True
        pageData = {
            'left': left,
            'right': right,
            'left_has_more': left_has_more,
            'right_has_more': right_has_more,
            'first': first,
            'last': last,
            'total_pages': total_pages,
            'page': page,
        }
    return render(
        request, 'note.html', {
            'active_menu': 'service',
            'sub_menu': submenu,
            'noteList': noteList,
            'pageData': pageData,
        })

def noteDetail(request, id):
    mynote = get_object_or_404(Mynote, id=id)  # 从MyNew表中获取指定id号的新闻
    mynote.views += 1
    mynote.save()
    return render(request, 'noteDetail.html', {
        'mynote': mynote,  # 传递给前端的模板变量
    })

def search(request):
    keyword=request.GET.get('keyword')
    noteList=Mynote.objects.filter(title__icontains=keyword) #标题中包含指定关键字
    noteName="关于"+"\""+keyword+"\""+"的搜索结果"
    return render(request,'searchList2.html',{
        'active_menu':'news',
        'noteName':noteName,
        'noteList':noteList,
    })
