import openai

# 设置OpenAI的API密钥
openai.api_key = "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"

def chat_with_gpt(input_text):
    response = openai.Completion.create(
        engine="text-davinci-003",
        prompt=input_text,
        max_tokens=1024,
        n=1,
        stop=None,
        temperature=0.5,
    )

    if response.choices:
        indented_response = '\n'.join(['\t' + line for line in response.choices[0].text.strip().split('\n')])
        print(indented_response)
        return indented_response
    else:
        return None
