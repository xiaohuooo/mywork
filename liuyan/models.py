from django.db import models

# Create your models here.
from datetime import datetime
from django.utils import timezone


class Resume(models.Model):
    name = models.CharField(max_length=20, verbose_name='用户名')
    email = models.CharField(max_length=30, verbose_name='邮箱')
    choice = models.CharField(max_length=30, verbose_name='打分')
    sex = models.CharField(max_length=5, default='男', verbose_name='性别')
    experience = models.TextField(blank=True,
                                  null=True,
                                  verbose_name='您的留言')

    publishDate = models.DateTimeField(max_length=20,
                                       default=timezone.now,
                                       verbose_name='提交时间', null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = '留言板'
        verbose_name_plural = '留言板'