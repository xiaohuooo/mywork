
from django.contrib.sites import requests

from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login


from jishu.models import Myjishu
from django.db.models import Q   #查询函数

def login_view(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('')
        else:
            # 登录失败的处理逻辑，例如显示错误消息
            error_message = "用户名或密码不正确"
            return render(request, 'home.html', {'error_message': error_message})

    return render(request, 'home.html')




import requests
from django.shortcuts import render

def get_weather(request):
    if request.method == 'POST':
        print(request.POST)
        api_key = "S25gERTOkgOZvz9Qa"
        location = request.POST.get('city')  # 获取前端输入的城市名称
        print(location)
        api_url = f"https://api.seniverse.com/v3/weather/now.json?key={api_key}&location={location}"

        response = requests.get(api_url)
        data = response.json()

        if response.status_code == 200:
            result = data["results"][0]
            location = result["location"]
            now = result["now"]

            weather_info = {
                "city": location["name"],
                "country": location["country"],
                "text": now["text"],
                "temperature": now["temperature"],
                # "feels_like": now["feels_like"],
                # "pressure": now["pressure"],
                # "wind_direction": now["wind_direction"],
                # "visibility": now["visibility"],
                # "humidity": now["humidity"],
            }

            return render(request, 'home.html', {'weather_info': weather_info})
        else:
            error_message = "请求失败"
            return render(request, 'home.html', {'error_message': error_message})
        # 新闻里，不是通知公告的
    jishuList = Myjishu.objects.all().filter(~Q(jishuType='Python')).order_by('-publishDate')
    postList = set()
    postNum = 0
    for s in jishuList:
        if s.photo:  # 有展报
            postList.add(s)
            postNum += 1
            print('有')
        if postNum == 3:  # 只截取最近的3个展报
            break

    # 新闻列表
    if (len(jishuList) > 7):
        jishuList = jishuList[0:7]  # 7条新闻

    noteList = Myjishu.objects.all().filter(
        ~Q(jishuType='Nosql')).order_by('-publishDate')
    if (len(noteList) > 4):
        noteList = noteList[0:4]
    return render(request, 'home.html',
                  {'active_menu': 'home',
                   'postList': postList,
                   'jishuList': jishuList,
                   'noteList': noteList,
                   })


