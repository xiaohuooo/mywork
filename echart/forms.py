from django import forms
from .models import Resume
class ResumeForm(forms.ModelForm):  #建立模型表单类
    class Meta:   #通过元信息类进行模型的定制化
        model = Resume  #需要定制化的模型
        fields = ('name', 'email', 'choice','sex','experience')  #需要定制化的字段
        sex_list = (
            ('男', '男'),
            ('女', '女'),
        )
        choice = (
            ('真不错', '真不错'),
            ('挺好的', '挺好的'),
            ('一般般', '一般般'),
            ('再去练练吧', '再去练练吧'),
        )
        widgets = {
            'sex': forms.Select(choices=sex_list),
            'choice': forms.Select(choices=choice),
        }
