from django.urls import path
from . import views

app_name='jishu'

urlpatterns=[
    path('jishu/<str:jishuName>', views.jishu, name='jishu'),
    path('jishuDetail/<int:id>/', views.jishuDetail, name='jishuDetail'),
    path('search/',views.search,name='search')
]