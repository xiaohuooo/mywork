$(function () {
  let country = "Afghanistan";
  echarts_1();
  echarts_2();
  echarts_4();
  echarts_31();
  countryList();
  countAll();
  //筛选按钮
  document.getElementById("filter1").addEventListener("click", function () {
    country = $("#country option:selected").val();
    console.log(country, "-countrycountry");
    echarts_1();
    echarts_2();
    echarts_4();
    // echarts_31();
    countAll();
  });
  //   echarts_5();
  //   echarts_6();
  function countryList(params) {
    axios({
      method: "get",
      url: "/echart/countryList",
    }).then((res) => {
      country = res.data[0];
      res.data.forEach((item) => {
        let name = item || "其他";

        $("#country").append(`<option value='${name}'>${name}</option>`); //新增
      });
    });
  }
  function countAll() {
    axios({
      method: "get",
      url: "/echart/countAll",
      params: {
        year: 1950,
        type: "Population",
        country,
      },
    }).then((res) => {
      document.querySelector(".leftcount").textContent =
        res.data.total_population;
    });
    axios({
      method: "get",
      url: "/echart/countAll",
      params: {
        year: 2021,
        type: "Population",
        country,
      },
    }).then((res) => {
      document.querySelector(".rightcount").textContent =
        res.data.total_population;
    });
  }
  function echarts_1() {
    axios({
      method: "get",
      url: "/echart/echarts1",
      params: {
        start_year: 1950,
        end_year: 2021,
        type: "Population_aged_15_to_64_years",
        country,
      },
    }).then((res) => {
      console.log(res.data);
      // 基于准备好的dom，初始化echarts实例
      var myChart = echarts.init(document.getElementById("echart1"));

      option = {
        //  backgroundColor: '#00265f',
        tooltip: {
          trigger: "axis",
          axisPointer: {
            type: "shadow",
          },
        },
        grid: {
          left: "0%",
          top: "10px",
          right: "4%",
          bottom: "4%",
          containLabel: true,
        },
        xAxis: [
          {
            type: "category",
            boundaryGap: false,
            data: res.data.map((item, index) => ""),
            axisLine: {
              show: true,
              lineStyle: {
                color: "rgba(255,255,255,.1)",
                width: 1,
                type: "solid",
              },
            },

            axisTick: {
              show: false,
            },
            axisLabel: {
              interval: 0,
              // rotate:50,
              show: true,
              splitNumber: 15,
              textStyle: {
                color: "rgba(255,255,255,.6)",
                fontSize: "12",
              },
            },
          },
        ],
        yAxis: [
          {
            type: "value",
            axisLabel: {
              //formatter: '{value} %'
              show: true,
              textStyle: {
                color: "rgba(255,255,255,.6)",
                fontSize: "12",
              },
            },
            axisTick: {
              show: false,
            },
            axisLine: {
              show: true,
              lineStyle: {
                color: "rgba(255,255,255,.1	)",
                width: 1,
                type: "solid",
              },
            },
            splitLine: {
              lineStyle: {
                color: "rgba(255,255,255,.1)",
              },
            },
          },
        ],
        series: [
          {
            type: "line",
            data: res.data.map((item) => item.total_population),
            smooth: true,
            symbol: "circle",
            symbolSize: 5,
            showSymbol: true,
            lineStyle: {
              normal: {
                color: "#0184d5",
                width: 2,
              },
            },
            areaStyle: {
              normal: {
                color: new echarts.graphic.LinearGradient(
                  0,
                  0,
                  0,
                  1,
                  [
                    {
                      offset: 0,
                      color: "rgba(1, 132, 213, 0.4)",
                    },
                    {
                      offset: 0.8,
                      color: "rgba(1, 132, 213, 0.1)",
                    },
                  ],
                  false
                ),
                shadowColor: "rgba(0, 0, 0, 0.1)",
              },
            },
            itemStyle: {
              normal: {
                color: "#0184d5",
                borderColor: "rgba(221, 220, 107, .1)",
                borderWidth: 12,
              },
            },
          },
        ],
      };

      // 使用刚指定的配置项和数据显示图表。
      myChart.setOption(option);
      window.addEventListener("resize", function () {
        myChart.resize();
      });
    });
  }
  function echarts_2() {
    // 基于准备好的dom，初始化echarts实例
    var myChart = echarts.init(document.getElementById("echart2"));
    axios({
      method: "get",
      url: "/echart/echarts1",
      params: {
        start_year: 1950,
        end_year: 2021,
        type: "Population_of_children_under_the_age_of_1",
        country,
      },
    }).then((res) => {
      option = {
        //  backgroundColor: '#00265f',
        tooltip: {
          trigger: "axis",
          axisPointer: { type: "shadow" },
        },
        grid: {
          left: "0%",
          top: "10px",
          right: "0%",
          bottom: "4%",
          containLabel: true,
        },
        xAxis: [
          {
            type: "category",
            data: res.data.map((item, index) => ""),
            axisLine: {
              show: true,
              lineStyle: {
                color: "rgba(255,255,255,.1)",
                width: 1,
                type: "solid",
              },
            },

            axisTick: {
              show: false,
            },
            axisLabel: {
              interval: 0,
              // rotate:50,
              show: true,
              splitNumber: 15,
              textStyle: {
                color: "rgba(255,255,255,.6)",
                fontSize: "12",
              },
            },
          },
        ],
        yAxis: [
          {
            type: "value",
            axisLabel: {
              //formatter: '{value} %'
              show: true,
              textStyle: {
                color: "rgba(255,255,255,.6)",
                fontSize: "12",
              },
            },
            axisTick: {
              show: false,
            },
            axisLine: {
              show: true,
              lineStyle: {
                color: "rgba(255,255,255,.1	)",
                width: 1,
                type: "solid",
              },
            },
            splitLine: {
              lineStyle: {
                color: "rgba(255,255,255,.1)",
              },
            },
          },
        ],
        series: [
          {
            type: "line",
            data: res.data.map((item) => item.total_population),
            barWidth: "35%", //柱子宽度
            // barGap: 1, //柱子之间间距
            itemStyle: {
              normal: {
                color: "#27d08a",
                opacity: 1,
                barBorderRadius: 5,
              },
            },
          },
        ],
      };

      // 使用刚指定的配置项和数据显示图表。
      myChart.setOption(option);
      window.addEventListener("resize", function () {
        myChart.resize();
      });
    });
  }
  function echarts_5() {
    // 基于准备好的dom，初始化echarts实例
    var myChart = echarts.init(document.getElementById("echart5"));
    axios({
      method: "get",
      url: "/echart/echarts5",
      params: {
        start_year: 2017,
        end_year: 2020,
      },
    }).then((res) => {
      option = {
        //  backgroundColor: '#00265f',
        tooltip: {
          trigger: "axis",
          axisPointer: {
            type: "shadow",
          },
        },

        grid: {
          left: "0%",
          top: "10px",
          right: "0%",
          bottom: "2%",
          containLabel: true,
        },
        xAxis: [
          {
            type: "category",
            data: res.data.map((item) => item.Year),
            axisLine: {
              show: true,
              lineStyle: {
                color: "rgba(255,255,255,.1)",
                width: 1,
                type: "solid",
              },
            },

            axisTick: {
              show: false,
            },
            axisLabel: {
              interval: 0,
              // rotate:50,
              show: true,
              splitNumber: 15,
              textStyle: {
                color: "rgba(255,255,255,.6)",
                fontSize: "12",
              },
            },
          },
        ],
        yAxis: [
          {
            type: "value",
            axisLabel: {
              //formatter: '{value} %'
              show: true,
              textStyle: {
                color: "rgba(255,255,255,.6)",
                fontSize: "12",
              },
            },
            axisTick: {
              show: false,
            },
            axisLine: {
              show: true,
              lineStyle: {
                color: "rgba(255,255,255,.1	)",
                width: 1,
                type: "solid",
              },
            },
            splitLine: {
              lineStyle: {
                color: "rgba(255,255,255,.1)",
              },
            },
          },
        ],
        series: [
          {
            type: "bar",
            data: res.data.map((item) => item.total_population),
            barWidth: "35%", //柱子宽度
            // barGap: 1, //柱子之间间距
            itemStyle: {
              normal: {
                color: "#2f89cf",
                opacity: 1,
                barBorderRadius: 5,
              },
            },
          },
        ],
      };

      // 使用刚指定的配置项和数据显示图表。
      myChart.setOption(option);
      window.addEventListener("resize", function () {
        myChart.resize();
      });
    });
  }

  function echarts_4() {
    // 基于准备好的dom，初始化echarts实例

    let allData = [
      {
        year: 2017,
        mouth: 1,
        type: "大工业",
        data: 43138852.38,
      },
      {
        year: 2017,
        mouth: 1,
        type: "非工业",
        data: 33905754.39,
      },
      {
        year: 2017,
        mouth: 1,
        type: "非居民",
        data: 1862780.71,
      },
      {
        year: 2017,
        mouth: 1,
        type: "居民",
        data: 22292901.59,
      },
      {
        year: 2017,
        mouth: 1,
        type: "农业",
        data: 490817.75,
      },
      {
        year: 2017,
        mouth: 1,
        type: "售外省",
        data: 2553888.7,
      },
      {
        year: 2017,
        mouth: 2,
        type: "大工业",
        data: 42100075.58,
      },
      {
        year: 2017,
        mouth: 2,
        type: "非工业",
        data: 30018018.13,
      },
      {
        year: 2017,
        mouth: 2,
        type: "非居民",
        data: 1809641.43,
      },
      {
        year: 2017,
        mouth: 2,
        type: "居民",
        data: 24843172.96,
      },
      {
        year: 2017,
        mouth: 2,
        type: "农业",
        data: 487026.84,
      },
      {
        year: 2017,
        mouth: 2,
        type: "售外省",
        data: 2322840.7,
      },
      {
        year: 2017,
        mouth: 3,
        type: "大工业",
        data: 45854398.53,
      },
      {
        year: 2017,
        mouth: 3,
        type: "非工业",
        data: 32004801.97,
      },
      {
        year: 2017,
        mouth: 3,
        type: "非居民",
        data: 1650165.75,
      },
      {
        year: 2017,
        mouth: 3,
        type: "居民",
        data: 23136330.09,
      },
      {
        year: 2017,
        mouth: 3,
        type: "农业",
        data: 478008.17,
      },
      {
        year: 2017,
        mouth: 3,
        type: "售外省",
        data: 2094206.5,
      },
      {
        year: 2017,
        mouth: 4,
        type: "大工业",
        data: 38655266.73,
      },
      {
        year: 2017,
        mouth: 4,
        type: "非工业",
        data: 19556155.48,
      },
      {
        year: 2017,
        mouth: 4,
        type: "非居民",
        data: 1201857.5,
      },
      {
        year: 2017,
        mouth: 4,
        type: "居民",
        data: 15969718.73,
      },
      {
        year: 2017,
        mouth: 4,
        type: "农业",
        data: 449586.82,
      },
      {
        year: 2017,
        mouth: 4,
        type: "售外省",
        data: 2253482.2,
      },
      {
        year: 2017,
        mouth: 5,
        type: "大工业",
        data: 44070773.41,
      },
      {
        year: 2017,
        mouth: 5,
        type: "非工业",
        data: 29649332.09,
      },
      {
        year: 2017,
        mouth: 5,
        type: "非居民",
        data: 1345436.67,
      },
      {
        year: 2017,
        mouth: 5,
        type: "居民",
        data: 15566939.41,
      },
      {
        year: 2017,
        mouth: 5,
        type: "农业",
        data: 465635.92,
      },
      {
        year: 2017,
        mouth: 5,
        type: "售外省",
        data: 2322081.4,
      },
      {
        year: 2017,
        mouth: 6,
        type: "大工业",
        data: 48138675.02,
      },
      {
        year: 2017,
        mouth: 6,
        type: "非工业",
        data: 33289088.5,
      },
      {
        year: 2017,
        mouth: 6,
        type: "非居民",
        data: 1465149.79,
      },
      {
        year: 2017,
        mouth: 6,
        type: "居民",
        data: 15262237.33,
      },
      {
        year: 2017,
        mouth: 6,
        type: "农业",
        data: 557755.15,
      },
      {
        year: 2017,
        mouth: 6,
        type: "售外省",
        data: 2424974.9,
      },
      {
        year: 2017,
        mouth: 7,
        type: "大工业",
        data: 64757555.27,
      },
      {
        year: 2017,
        mouth: 7,
        type: "非工业",
        data: 53278043.3,
      },
      {
        year: 2017,
        mouth: 7,
        type: "非居民",
        data: 1976919.52,
      },
      {
        year: 2017,
        mouth: 7,
        type: "居民",
        data: 17769938.57,
      },
      {
        year: 2017,
        mouth: 7,
        type: "农业",
        data: 764878.47,
      },
      {
        year: 2017,
        mouth: 7,
        type: "售外省",
        data: 2869700.8,
      },
      {
        year: 2017,
        mouth: 8,
        type: "大工业",
        data: 52252863.59,
      },
      {
        year: 2017,
        mouth: 8,
        type: "非工业",
        data: 48589792.93,
      },
      {
        year: 2017,
        mouth: 8,
        type: "非居民",
        data: 2167116.4,
      },
      {
        year: 2017,
        mouth: 8,
        type: "居民",
        data: 43832067.59,
      },
      {
        year: 2017,
        mouth: 8,
        type: "农业",
        data: 1007168.35,
      },
      {
        year: 2017,
        mouth: 8,
        type: "售外省",
        data: 2691738.5,
      },
      {
        year: 2017,
        mouth: 9,
        type: "大工业",
        data: 37065272.61,
      },
      {
        year: 2017,
        mouth: 9,
        type: "非工业",
        data: 30297632.33,
      },
      {
        year: 2017,
        mouth: 9,
        type: "非居民",
        data: 1537878.92,
      },
      {
        year: 2017,
        mouth: 9,
        type: "居民",
        data: 36601504.15,
      },
      {
        year: 2017,
        mouth: 9,
        type: "农业",
        data: 817097.9,
      },
      {
        year: 2017,
        mouth: 9,
        type: "售外省",
        data: 2367172.2,
      },
      {
        year: 2017,
        mouth: 10,
        type: "大工业",
        data: 42263429.2,
      },
      {
        year: 2017,
        mouth: 10,
        type: "非工业",
        data: 29438267.62,
      },
      {
        year: 2017,
        mouth: 10,
        type: "非居民",
        data: 1504723.8,
      },
      {
        year: 2017,
        mouth: 10,
        type: "居民",
        data: 18063809.39,
      },
      {
        year: 2017,
        mouth: 10,
        type: "农业",
        data: 679956.82,
      },
      {
        year: 2017,
        mouth: 10,
        type: "售外省",
        data: 2072235.9,
      },
      {
        year: 2017,
        mouth: 11,
        type: "大工业",
        data: 47109063.46,
      },
      {
        year: 2017,
        mouth: 11,
        type: "非工业",
        data: 30707260.58,
      },
      {
        year: 2017,
        mouth: 11,
        type: "非居民",
        data: 1499452.99,
      },
      {
        year: 2017,
        mouth: 11,
        type: "居民",
        data: 15497186.94,
      },
      {
        year: 2017,
        mouth: 11,
        type: "农业",
        data: 517837.3,
      },
      {
        year: 2017,
        mouth: 11,
        type: "售外省",
        data: 4721880.9,
      },
      {
        year: 2017,
        mouth: 12,
        type: "大工业",
        data: 54323372.04,
      },
      {
        year: 2017,
        mouth: 12,
        type: "非工业",
        data: 36443400.93,
      },
      {
        year: 2017,
        mouth: 12,
        type: "非居民",
        data: 1775033.11,
      },
      {
        year: 2017,
        mouth: 12,
        type: "居民",
        data: 18088036.03,
      },
      {
        year: 2017,
        mouth: 12,
        type: "农业",
        data: 488712.59,
      },
      {
        year: 2017,
        mouth: 12,
        type: "售外省",
        data: 2151544,
      },
      {
        year: 2018,
        mouth: 1,
        type: "大工业",
        data: 55479679.3,
      },
      {
        year: 2018,
        mouth: 1,
        type: "非工业",
        data: 38894126.81,
      },
      {
        year: 2018,
        mouth: 1,
        type: "非居民",
        data: 2095905.82,
      },
      {
        year: 2018,
        mouth: 1,
        type: "居民",
        data: 27316873.29,
      },
      {
        year: 2018,
        mouth: 1,
        type: "农业",
        data: 558030.8,
      },
      {
        year: 2018,
        mouth: 1,
        type: "售外省",
        data: 2640456.7,
      },
      {
        year: 2018,
        mouth: 2,
        type: "大工业",
        data: 33704505.78,
      },
      {
        year: 2018,
        mouth: 2,
        type: "非工业",
        data: 29836945.72,
      },
      {
        year: 2018,
        mouth: 2,
        type: "非居民",
        data: 1771012.4,
      },
      {
        year: 2018,
        mouth: 2,
        type: "居民",
        data: 33400603.3,
      },
      {
        year: 2018,
        mouth: 2,
        type: "农业",
        data: 491393.94,
      },
      {
        year: 2018,
        mouth: 2,
        type: "售外省",
        data: 2577336.1,
      },
      {
        year: 2018,
        mouth: 3,
        type: "大工业",
        data: 43217837.74,
      },
      {
        year: 2018,
        mouth: 3,
        type: "非工业",
        data: 33739774.67,
      },
      {
        year: 2018,
        mouth: 3,
        type: "非居民",
        data: 1724118.07,
      },
      {
        year: 2018,
        mouth: 3,
        type: "居民",
        data: 27025450.12,
      },
      {
        year: 2018,
        mouth: 3,
        type: "农业",
        data: 454384.1,
      },
      {
        year: 2018,
        mouth: 3,
        type: "售外省",
        data: 4634294.2,
      },
      {
        year: 2018,
        mouth: 4,
        type: "大工业",
        data: 40899696.66,
      },
      {
        year: 2018,
        mouth: 4,
        type: "非工业",
        data: 28493316.5,
      },
      {
        year: 2018,
        mouth: 4,
        type: "非居民",
        data: 995501.73,
      },
      {
        year: 2018,
        mouth: 4,
        type: "居民",
        data: 20547833.4,
      },
      {
        year: 2018,
        mouth: 4,
        type: "农业",
        data: 462227.78,
      },
      {
        year: 2018,
        mouth: 4,
        type: "售外省",
        data: 2490426.6,
      },
      {
        year: 2018,
        mouth: 5,
        type: "大工业",
        data: 48946814.15,
      },
      {
        year: 2018,
        mouth: 5,
        type: "非工业",
        data: 33318793.47,
      },
      {
        year: 2018,
        mouth: 5,
        type: "非居民",
        data: 491841.36,
      },
      {
        year: 2018,
        mouth: 5,
        type: "居民",
        data: 16123859.31,
      },
      {
        year: 2018,
        mouth: 5,
        type: "农业",
        data: 502177.22,
      },
      {
        year: 2018,
        mouth: 5,
        type: "售外省",
        data: 3016322.7,
      },
      {
        year: 2018,
        mouth: 6,
        type: "大工业",
        data: 46616492.02,
      },
      {
        year: 2018,
        mouth: 6,
        type: "非工业",
        data: 35513890.19,
      },
      {
        year: 2018,
        mouth: 6,
        type: "非居民",
        data: 494878.17,
      },
      {
        year: 2018,
        mouth: 6,
        type: "居民",
        data: 17254332.27,
      },
      {
        year: 2018,
        mouth: 6,
        type: "农业",
        data: 577777.59,
      },
      {
        year: 2018,
        mouth: 6,
        type: "售外省",
        data: 3145305.5,
      },
      {
        year: 2018,
        mouth: 7,
        type: "大工业",
        data: 61114618.76,
      },
      {
        year: 2018,
        mouth: 7,
        type: "非工业",
        data: 51629008.94,
      },
      {
        year: 2018,
        mouth: 7,
        type: "非居民",
        data: 431185.72,
      },
      {
        year: 2018,
        mouth: 7,
        type: "居民",
        data: 21114625.32,
      },
      {
        year: 2018,
        mouth: 7,
        type: "农业",
        data: 818252.66,
      },
      {
        year: 2018,
        mouth: 7,
        type: "售外省",
        data: 3749388.3,
      },
      {
        year: 2018,
        mouth: 8,
        type: "大工业",
        data: 55052136.88,
      },
      {
        year: 2018,
        mouth: 8,
        type: "非工业",
        data: 51521670.01,
      },
      {
        year: 2018,
        mouth: 8,
        type: "非居民",
        data: 467027.14,
      },
      {
        year: 2018,
        mouth: 8,
        type: "居民",
        data: 34769433.13,
      },
      {
        year: 2018,
        mouth: 8,
        type: "农业",
        data: 899738.06,
      },
      {
        year: 2018,
        mouth: 8,
        type: "售外省",
        data: 3860386.5,
      },
      {
        year: 2018,
        mouth: 9,
        type: "大工业",
        data: 44293964.41,
      },
      {
        year: 2018,
        mouth: 9,
        type: "非工业",
        data: 38757961.6,
      },
      {
        year: 2018,
        mouth: 9,
        type: "非居民",
        data: 478051.71,
      },
      {
        year: 2018,
        mouth: 9,
        type: "居民",
        data: 36376460.51,
      },
      {
        year: 2018,
        mouth: 9,
        type: "农业",
        data: 765287.16,
      },
      {
        year: 2018,
        mouth: 9,
        type: "售外省",
        data: 3687367.9,
      },
      {
        year: 2018,
        mouth: 10,
        type: "大工业",
        data: 40057672.03,
      },
      {
        year: 2018,
        mouth: 10,
        type: "非工业",
        data: 32343499.51,
      },
      {
        year: 2018,
        mouth: 10,
        type: "非居民",
        data: 517611.63,
      },
      {
        year: 2018,
        mouth: 10,
        type: "居民",
        data: 23081230.97,
      },
      {
        year: 2018,
        mouth: 10,
        type: "农业",
        data: 719332.2,
      },
      {
        year: 2018,
        mouth: 10,
        type: "售外省",
        data: 3652322.7,
      },
      {
        year: 2018,
        mouth: 11,
        type: "大工业",
        data: 45266826.24,
      },
      {
        year: 2018,
        mouth: 11,
        type: "非工业",
        data: 33601988.97,
      },
      {
        year: 2018,
        mouth: 11,
        type: "非居民",
        data: 706231.69,
      },
      {
        year: 2018,
        mouth: 11,
        type: "居民",
        data: 15987384.98,
      },
      {
        year: 2018,
        mouth: 11,
        type: "农业",
        data: 575077.63,
      },
      {
        year: 2018,
        mouth: 11,
        type: "售外省",
        data: 3094573.4,
      },
      {
        year: 2018,
        mouth: 12,
        type: "大工业",
        data: 52169053.8,
      },
      {
        year: 2018,
        mouth: 12,
        type: "非工业",
        data: 42224449.7,
      },
      {
        year: 2018,
        mouth: 12,
        type: "非居民",
        data: 580941.35,
      },
      {
        year: 2018,
        mouth: 12,
        type: "居民",
        data: 18060633.08,
      },
      {
        year: 2018,
        mouth: 12,
        type: "农业",
        data: 494056.06,
      },
      {
        year: 2018,
        mouth: 12,
        type: "售外省",
        data: 3072451,
      },
      {
        year: 2019,
        mouth: 1,
        type: "大工业",
        data: 53446130.42,
      },
      {
        year: 2019,
        mouth: 1,
        type: "非工业",
        data: 47192203.2,
      },
      {
        year: 2019,
        mouth: 1,
        type: "非居民",
        data: 635379.23,
      },
      {
        year: 2019,
        mouth: 1,
        type: "居民",
        data: 30695639.61,
      },
      {
        year: 2019,
        mouth: 1,
        type: "农业",
        data: 577326.49,
      },
      {
        year: 2019,
        mouth: 1,
        type: "售外省",
        data: 3369228.1,
      },
      {
        year: 2019,
        mouth: 2,
        type: "大工业",
        data: 36904928.47,
      },
      {
        year: 2019,
        mouth: 2,
        type: "非工业",
        data: 31727106.89,
      },
      {
        year: 2019,
        mouth: 2,
        type: "非居民",
        data: 617566.62,
      },
      {
        year: 2019,
        mouth: 2,
        type: "居民",
        data: 34425750.2,
      },
      {
        year: 2019,
        mouth: 2,
        type: "农业",
        data: 485619.31,
      },
      {
        year: 2019,
        mouth: 2,
        type: "售外省",
        data: 2573416.5,
      },
      {
        year: 2019,
        mouth: 3,
        type: "大工业",
        data: 42492705.25,
      },
      {
        year: 2019,
        mouth: 3,
        type: "非工业",
        data: 35713603.63,
      },
      {
        year: 2019,
        mouth: 3,
        type: "非居民",
        data: 535398.25,
      },
      {
        year: 2019,
        mouth: 3,
        type: "居民",
        data: 29249960.05,
      },
      {
        year: 2019,
        mouth: 3,
        type: "农业",
        data: 435714.08,
      },
      {
        year: 2019,
        mouth: 3,
        type: "售外省",
        data: 2175807.3,
      },
      {
        year: 2019,
        mouth: 4,
        type: "大工业",
        data: 41900921.38,
      },
      {
        year: 2019,
        mouth: 4,
        type: "非工业",
        data: 29741585.08,
      },
      {
        year: 2019,
        mouth: 4,
        type: "非居民",
        data: 566772.21,
      },
      {
        year: 2019,
        mouth: 4,
        type: "居民",
        data: 23363964.47,
      },
      {
        year: 2019,
        mouth: 4,
        type: "农业",
        data: 483273.91,
      },
      {
        year: 2019,
        mouth: 4,
        type: "售外省",
        data: 2162852.3,
      },
      {
        year: 2019,
        mouth: 5,
        type: "大工业",
        data: 45926761.28,
      },
      {
        year: 2019,
        mouth: 5,
        type: "非工业",
        data: 33873523.95,
      },
      {
        year: 2019,
        mouth: 5,
        type: "非居民",
        data: 518853.86,
      },
      {
        year: 2019,
        mouth: 5,
        type: "居民",
        data: 17161994.56,
      },
      {
        year: 2019,
        mouth: 5,
        type: "农业",
        data: 448147.53,
      },
      {
        year: 2019,
        mouth: 5,
        type: "售外省",
        data: 2615281.4,
      },
      {
        year: 2019,
        mouth: 6,
        type: "大工业",
        data: 49428532.72,
      },
      {
        year: 2019,
        mouth: 6,
        type: "非工业",
        data: 38610466.05,
      },
      {
        year: 2019,
        mouth: 6,
        type: "非居民",
        data: 493557.23,
      },
      {
        year: 2019,
        mouth: 6,
        type: "居民",
        data: 17154426.14,
      },
      {
        year: 2019,
        mouth: 6,
        type: "农业",
        data: 581175.76,
      },
      {
        year: 2019,
        mouth: 6,
        type: "售外省",
        data: 2942168.1,
      },
      {
        year: 2019,
        mouth: 7,
        type: "大工业",
        data: 76237842.26,
      },
      {
        year: 2019,
        mouth: 7,
        type: "非工业",
        data: 56284199.57,
      },
      {
        year: 2019,
        mouth: 7,
        type: "非居民",
        data: 475274.73,
      },
      {
        year: 2019,
        mouth: 7,
        type: "居民",
        data: 19542609.58,
      },
      {
        year: 2019,
        mouth: 7,
        type: "农业",
        data: 881987.1,
      },
      {
        year: 2019,
        mouth: 7,
        type: "售外省",
        data: 3744852.2,
      },
      {
        year: 2019,
        mouth: 8,
        type: "大工业",
        data: 54073985.14,
      },
      {
        year: 2019,
        mouth: 8,
        type: "非工业",
        data: 54081882.47,
      },
      {
        year: 2019,
        mouth: 8,
        type: "非居民",
        data: 494740.83,
      },
      {
        year: 2019,
        mouth: 8,
        type: "居民",
        data: 31431791.76,
      },
      {
        year: 2019,
        mouth: 8,
        type: "农业",
        data: 858766.46,
      },
      {
        year: 2019,
        mouth: 8,
        type: "售外省",
        data: 3718030,
      },
      {
        year: 2019,
        mouth: 9,
        type: "大工业",
        data: 41541792.54,
      },
      {
        year: 2019,
        mouth: 9,
        type: "非工业",
        data: 39526036.98,
      },
      {
        year: 2019,
        mouth: 9,
        type: "非居民",
        data: 503347.56,
      },
      {
        year: 2019,
        mouth: 9,
        type: "居民",
        data: 36679708.95,
      },
      {
        year: 2019,
        mouth: 9,
        type: "农业",
        data: 797130.21,
      },
      {
        year: 2019,
        mouth: 9,
        type: "售外省",
        data: 3632343.9,
      },
      {
        year: 2019,
        mouth: 10,
        type: "大工业",
        data: 44509431.27,
      },
      {
        year: 2019,
        mouth: 10,
        type: "非工业",
        data: 37838283.19,
      },
      {
        year: 2019,
        mouth: 10,
        type: "非居民",
        data: 535969.62,
      },
      {
        year: 2019,
        mouth: 10,
        type: "居民",
        data: 20533978.79,
      },
      {
        year: 2019,
        mouth: 10,
        type: "农业",
        data: 688047.83,
      },
      {
        year: 2019,
        mouth: 10,
        type: "售外省",
        data: 2913379.3,
      },
      {
        year: 2019,
        mouth: 11,
        type: "大工业",
        data: 47071614.98,
      },
      {
        year: 2019,
        mouth: 11,
        type: "非工业",
        data: 33581491.99,
      },
      {
        year: 2019,
        mouth: 11,
        type: "非居民",
        data: 668969.15,
      },
      {
        year: 2019,
        mouth: 11,
        type: "居民",
        data: 16795113.64,
      },
      {
        year: 2019,
        mouth: 11,
        type: "农业",
        data: 501616.05,
      },
      {
        year: 2019,
        mouth: 11,
        type: "售外省",
        data: 2406748.1,
      },
      {
        year: 2019,
        mouth: 12,
        type: "大工业",
        data: 52966877.69,
      },
      {
        year: 2019,
        mouth: 12,
        type: "非工业",
        data: 44705035.41,
      },
      {
        year: 2019,
        mouth: 12,
        type: "非居民",
        data: 609793.88,
      },
      {
        year: 2019,
        mouth: 12,
        type: "居民",
        data: 19560075.59,
      },
      {
        year: 2019,
        mouth: 12,
        type: "农业",
        data: 528680.58,
      },
      {
        year: 2019,
        mouth: 12,
        type: "售外省",
        data: 2851522.2,
      },
      {
        year: 2020,
        mouth: 1,
        type: "大工业",
        data: 47908488.23,
      },
      {
        year: 2020,
        mouth: 1,
        type: "非工业",
        data: 40946540.99,
      },
      {
        year: 2020,
        mouth: 1,
        type: "非居民",
        data: 678025.59,
      },
      {
        year: 2020,
        mouth: 1,
        type: "居民",
        data: 28170322.41,
      },
      {
        year: 2020,
        mouth: 1,
        type: "农业",
        data: 429522.82,
      },
      {
        year: 2020,
        mouth: 1,
        type: "售外省",
        data: 2670336.9,
      },
      {
        year: 2020,
        mouth: 2,
        type: "大工业",
        data: 32578760.98,
      },
      {
        year: 2020,
        mouth: 2,
        type: "非工业",
        data: 26932878.24,
      },
      {
        year: 2020,
        mouth: 2,
        type: "非居民",
        data: 608446.75,
      },
      {
        year: 2020,
        mouth: 2,
        type: "居民",
        data: 29449755.3,
      },
      {
        year: 2020,
        mouth: 2,
        type: "农业",
        data: 372008.05,
      },
      {
        year: 2020,
        mouth: 2,
        type: "售外省",
        data: 2384085.6,
      },
      {
        year: 2020,
        mouth: 3,
        type: "大工业",
        data: 38847212.64,
      },
      {
        year: 2020,
        mouth: 3,
        type: "非工业",
        data: 28728151.39,
      },
      {
        year: 2020,
        mouth: 3,
        type: "非居民",
        data: 594597.99,
      },
      {
        year: 2020,
        mouth: 3,
        type: "居民",
        data: 28644320.21,
      },
      {
        year: 2020,
        mouth: 3,
        type: "农业",
        data: 472865.85,
      },
      {
        year: 2020,
        mouth: 3,
        type: "售外省",
        data: 3576091.5,
      },
      {
        year: 2020,
        mouth: 4,
        type: "大工业",
        data: 45407712.32,
      },
      {
        year: 2020,
        mouth: 4,
        type: "非工业",
        data: 30444626.21,
      },
      {
        year: 2020,
        mouth: 4,
        type: "非居民",
        data: 578340.23,
      },
      {
        year: 2020,
        mouth: 4,
        type: "居民",
        data: 22555717.9,
      },
      {
        year: 2020,
        mouth: 4,
        type: "农业",
        data: 431596.79,
      },
      {
        year: 2020,
        mouth: 4,
        type: "售外省",
        data: 2824067,
      },
      {
        year: 2020,
        mouth: 5,
        type: "大工业",
        data: 45506662.44,
      },
      {
        year: 2020,
        mouth: 5,
        type: "非工业",
        data: 31715147.55,
      },
      {
        year: 2020,
        mouth: 5,
        type: "非居民",
        data: 537550.36,
      },
      {
        year: 2020,
        mouth: 5,
        type: "居民",
        data: 18164338.48,
      },
      {
        year: 2020,
        mouth: 5,
        type: "农业",
        data: 476143.39,
      },
      {
        year: 2020,
        mouth: 5,
        type: "售外省",
        data: 3521653.3,
      },
      {
        year: 2020,
        mouth: 6,
        type: "大工业",
        data: 49113266.25,
      },
      {
        year: 2020,
        mouth: 6,
        type: "非工业",
        data: 41658860.45,
      },
      {
        year: 2020,
        mouth: 6,
        type: "非居民",
        data: 524252.19,
      },
      {
        year: 2020,
        mouth: 6,
        type: "居民",
        data: 18380337.4,
      },
      {
        year: 2020,
        mouth: 6,
        type: "农业",
        data: 670211.9,
      },
      {
        year: 2020,
        mouth: 6,
        type: "售外省",
        data: 3322589,
      },
      {
        year: 2020,
        mouth: 7,
        type: "大工业",
        data: 52715245.43,
      },
      {
        year: 2020,
        mouth: 7,
        type: "非工业",
        data: 44234610.49,
      },
      {
        year: 2020,
        mouth: 7,
        type: "非居民",
        data: 464441.49,
      },
      {
        year: 2020,
        mouth: 7,
        type: "居民",
        data: 22574038.44,
      },
      {
        year: 2020,
        mouth: 7,
        type: "农业",
        data: 693932.56,
      },
      {
        year: 2020,
        mouth: 7,
        type: "售外省",
        data: 3741623,
      },
      {
        year: 2020,
        mouth: 8,
        type: "大工业",
        data: 64202744.45,
      },
      {
        year: 2020,
        mouth: 8,
        type: "非工业",
        data: 61882084.22,
      },
      {
        year: 2020,
        mouth: 8,
        type: "非居民",
        data: 497231.5,
      },
      {
        year: 2020,
        mouth: 8,
        type: "居民",
        data: 29798942.6,
      },
      {
        year: 2020,
        mouth: 8,
        type: "农业",
        data: 891587.23,
      },
      {
        year: 2020,
        mouth: 8,
        type: "售外省",
        data: 4055226.4,
      },
      {
        year: 2020,
        mouth: 9,
        type: "大工业",
        data: 39882970.28,
      },
      {
        year: 2020,
        mouth: 9,
        type: "非工业",
        data: 36726564.17,
      },
      {
        year: 2020,
        mouth: 9,
        type: "非居民",
        data: 526731.41,
      },
      {
        year: 2020,
        mouth: 9,
        type: "居民",
        data: 45296328.01,
      },
      {
        year: 2020,
        mouth: 9,
        type: "农业",
        data: 838175.1,
      },
      {
        year: 2020,
        mouth: 9,
        type: "售外省",
        data: 3777050.2,
      },
      {
        year: 2020,
        mouth: 10,
        type: "大工业",
        data: 45711589.99,
      },
      {
        year: 2020,
        mouth: 10,
        type: "非工业",
        data: 35536388.31,
      },
      {
        year: 2020,
        mouth: 10,
        type: "非居民",
        data: 565858.07,
      },
      {
        year: 2020,
        mouth: 10,
        type: "居民",
        data: 22272799.82,
      },
      {
        year: 2020,
        mouth: 10,
        type: "农业",
        data: 725391.09,
      },
      {
        year: 2020,
        mouth: 10,
        type: "售外省",
        data: 5011305.2,
      },
      {
        year: 2020,
        mouth: 11,
        type: "大工业",
        data: 49476846.74,
      },
      {
        year: 2020,
        mouth: 11,
        type: "非工业",
        data: 36604054.53,
      },
      {
        year: 2020,
        mouth: 11,
        type: "非居民",
        data: 765351.33,
      },
      {
        year: 2020,
        mouth: 11,
        type: "居民",
        data: 17559232.56,
      },
      {
        year: 2020,
        mouth: 11,
        type: "农业",
        data: 575054.86,
      },
      {
        year: 2020,
        mouth: 11,
        type: "售外省",
        data: 3689179.7,
      },
      {
        year: 2020,
        mouth: 12,
        type: "大工业",
        data: 60643100.48,
      },
      {
        year: 2020,
        mouth: 12,
        type: "非工业",
        data: 49739712.77,
      },
      {
        year: 2020,
        mouth: 12,
        type: "非居民",
        data: 622547.05,
      },
      {
        year: 2020,
        mouth: 12,
        type: "居民",
        data: 21550555.43,
      },
      {
        year: 2020,
        mouth: 12,
        type: "农业",
        data: 589076.9,
      },
      {
        year: 2020,
        mouth: 12,
        type: "售外省",
        data: 3808055.5,
      },
    ];
    //获取某个value并去重
    function getValues(arr, key) {
      let newArr = [];
      for (var i = 0; i < arr.length; i++) {
        for (var j = i + 1; j < arr.length; j++) {
          if (arr[i][key] === arr[j][key]) {
            ++i;
          }
        }
        newArr.push(arr[i][key]);
      }
      return [...new Set(newArr)];
    }

    let years = getValues(allData, "year");
    let mouths = getValues(allData, "mouth");
    let types = getValues(allData, "type");
    types = ["1岁以下儿童人口", "25岁以下人口", "15至64岁人口", "其他"];
    if (years != "" && years != null && types != "" && types != null) {
      $("#years2").empty()
      for (var i = 1950; i < 2022; i++) {
        $("#years2").append(`<option value='${i}'>${i}</option>`); //新增
      }
      $("#years2 option:eq(0)").attr("selected", "selected"); //选中第一个
      // for (var i = 0; i < types.length; i++) {
      //     $("#types2").append(`<option value='${i}'>${types[i]}</option>`); //新增
      // }
      // $("#types2 option:eq(0)").attr("selected", "selected"); //选中第一个
    }

    function getData(year, type) {
      let barData = [];
      allData.map(function (item) {
        if (item.type == type && item.year == year) {
          barData.push(item.data);
        }
      });
      return barData;
    }
    let Bardata0 = getData(years[0], types[0]);
    console.log(Bardata0, "-Bardata0Bardata0Bardata0");

    axios({
      method: "get",
      url: "/echart/echarts4",
      params: {
        year: 2017,
        // type: "1",
        country,
      },
    }).then((res) => {
      randerBar(res.data);
    });

    //筛选按钮
    document.getElementById("filter3").addEventListener("click", function () {
      let checkyear = $("#years2 option:selected").val();
      // let checkType = $("#types2 option:selected").val();
      axios({
        method: "get",
        url: "/echart/echarts4",
        params: {
          year: checkyear,
          country,
          // type: checkType,
        },
      }).then((res) => {
        randerBar(res.data);
      });
      //   randerBar(getData(checkyear, checkType));
    });
    function randerBar(data) {
      console.log(data, "---------data");
      var myChart = echarts.init(document.getElementById("echart4"));
      type_mapping = {
        Population_of_children_under_the_age_of_1: "0-1",
        Population_under_the_age_of_25: "0-25",
        Population_aged_15_to_64_years: "15-64",
        "Population aged 90 to 99 years": "90-99",
        "Population older than 100 years": "100",
        Population: "all",
      };
      option = {
        title: [
          {
            top: "20%",
            left: "5%",
            text: "1950/2021各类人口的人口量",

            textStyle: {
              color: "#fff",
              fontSize: "20",
              fontWeight: "normal",
            },
          },
        ],
        tooltip: {
          trigger: "axis",
          axisPointer: {
            lineStyle: {
              color: "#dddc6b",
            },
          },
          formatter: (params) => {
            var dataValue = params[0].value
            let arr = Object.keys(data)
              .splice(1)
            return (
              arr[params[0].axisValue] +
              '<br/>人口:' + dataValue
            )
          },
        },
        legend: {
          top: "20%",
          right: "0%",

          textStyle: {
            color: "rgba(255,255,255,.5)",
            fontSize: "12",
          },
        },
        grid: {
          left: "10",
          top: "35%",
          right: "10",
          bottom: "10",
          containLabel: true,
        },

        xAxis: [
          {
            type: "category",
            // boundaryGap: false,
            axisLabel: {
              textStyle: {
                color: "rgba(255,255,255,.6)",
                fontSize: 12,
              },
            },
            axisLine: {
              lineStyle: {
                color: "rgba(255,255,255,.2)",
              },
            },

            data: Object.keys(data)
              .splice(1)
              .map((item, index) => index),
          },
          {
            axisPointer: { show: false },
            axisLine: { show: false },
            position: "bottom",
            offset: 20,
          },
        ],

        yAxis: [
          {
            type: "value",
            axisTick: { show: false },
            axisLine: {
              lineStyle: {
                color: "rgba(255,255,255,.1)",
              },
            },
            axisLabel: {
              textStyle: {
                color: "rgba(255,255,255,.6)",
                fontSize: 12,
              },
            },

            splitLine: {
              lineStyle: {
                color: "rgba(255,255,255,.1)",
              },
            },
          },
        ],
        series: [
          {
            name: "人口",
            type: "bar",
            smooth: true,
            barWidth: "50%",
            itemStyle: {
              normal: {
                barBorderRadius: 5,
                color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                  {
                    offset: 0,
                    color: "#8bd46e",
                  },
                  {
                    offset: 1,
                    color: "#09bcb7",
                  },
                ]),
              },
            },
            barGap: "0.2",
            data: Object.keys(data)
              .splice(1)
              .map((item) => data[item]),
          },
        ],
      };

      // 使用刚指定的配置项和数据显示图表。
      myChart.setOption(option);
      window.addEventListener("resize", function () {
        myChart.resize();
      });
    }
  }
  function echarts_6() {
    // 基于准备好的dom，初始化echarts实例
    var myChart = echarts.init(document.getElementById("echart6"));

    var dataStyle = {
      normal: {
        label: {
          show: false,
        },
        labelLine: {
          show: false,
        },
        //shadowBlur: 40,
        //shadowColor: 'rgba(40, 40, 40, 1)',
      },
    };
    var placeHolderStyle = {
      normal: {
        color: "rgba(255,255,255,.05)",
        label: { show: false },
        labelLine: { show: false },
      },
      emphasis: {
        color: "rgba(0,0,0,0)",
      },
    };
    axios({
      method: "get",
      url: "/echart/echarts6",
      params: {
        start_year: 2017,
        end_year: 2020,
      },
    }).then((res) => {
      option = {
        color: [
          "#0f63d6",
          "#0f78d6",
          "#0f8cd6",
          "#0fa0d6",
          "#0fb4d6",
          "#0fb4d6",
        ],
        tooltip: {
          show: true,
          formatter: "{a} : {c} ",
        },
        legend: {
          itemWidth: 10,
          itemHeight: 10,
          itemGap: 12,
          bottom: "3%",

          data: res.data.map((item) => item.name || "其他"),
          textStyle: {
            color: "rgba(255,255,255,.6)",
          },
        },

        series: [
          {
            name: res.data[0].name || "其他",
            type: "pie",
            clockWise: false,
            center: ["50%", "42%"],
            radius: ["59%", "70%"],
            itemStyle: dataStyle,
            hoverAnimation: false,
            data: [
              {
                value: res.data[0].percentage,
                name: "01",
              },
              {
                value: 100 - res.data[0].percentage,
                name: "invisible",
                tooltip: { show: false },
                itemStyle: placeHolderStyle,
              },
            ],
          },
          {
            name: res.data[1].name || "其他",
            type: "pie",
            clockWise: false,
            center: ["50%", "42%"],
            radius: ["49%", "60%"],
            itemStyle: dataStyle,
            hoverAnimation: false,
            data: [
              {
                value: res.data[1].percentage,
                name: "02",
              },
              {
                value: 100 - res.data[1].percentage,
                name: "invisible",
                tooltip: { show: false },
                itemStyle: placeHolderStyle,
              },
            ],
          },
          {
            name: res.data[2].name || "其他",
            type: "pie",
            clockWise: false,
            hoverAnimation: false,
            center: ["50%", "42%"],
            radius: ["39%", "50%"],
            itemStyle: dataStyle,
            data: [
              {
                value: res.data[2].percentage,
                name: "03",
              },
              {
                value: 100 - res.data[2].percentage,
                name: "invisible",
                tooltip: { show: false },
                itemStyle: placeHolderStyle,
              },
            ],
          },
          {
            name: res.data[3].name || "其他",
            type: "pie",
            clockWise: false,
            center: ["50%", "42%"],
            radius: ["29%", "40%"],
            itemStyle: dataStyle,
            hoverAnimation: false,
            data: [
              {
                value: res.data[3].percentage,
                name: "04",
              },
              {
                value: 100 - res.data[3].percentage,
                name: "invisible",
                tooltip: { show: false },
                itemStyle: placeHolderStyle,
              },
            ],
          },
          {
            name: res.data[4].name || "其他",
            type: "pie",
            clockWise: false,
            hoverAnimation: false,
            center: ["50%", "42%"],
            radius: ["20%", "30%"],
            itemStyle: dataStyle,
            data: [
              {
                value: res.data[4].percentage,
                name: "05",
              },
              {
                value: 100 - res.data[4].percentage,
                name: "invisible",
                tooltip: { show: false },
                itemStyle: placeHolderStyle,
              },
            ],
          },
          {
            name: res.data[5].name || "其他",
            type: "pie",
            clockWise: false,
            hoverAnimation: false,
            center: ["50%", "42%"],
            radius: ["10%", "20%"],
            itemStyle: dataStyle,
            data: [
              {
                value: res.data[5].percentage,
                name: "06",
              },
              {
                value: 100 - res.data[5].percentage,
                name: "invisible",
                tooltip: { show: false },
                itemStyle: placeHolderStyle,
              },
            ],
          },
        ],
      };

      // 使用刚指定的配置项和数据显示图表。
      myChart.setOption(option);
      window.addEventListener("resize", function () {
        myChart.resize();
      });
    });
  }
  function echarts_31() {
    $("#yearSelector").empty()
    for (var i = 1950; i < 2022; i++) {
      $("#yearSelector").append(`<option value='${i}'>${i}</option>`); //新增
    }
    var myChart = echarts.init(document.getElementById("fb1"));
    var yearSelector = document.getElementById("yearSelector");
    axios({
      method: "get",
      url: "/echart/echarts31",
      params: {
        start_year: 1950,
        end_year: 2021,
      },
    }).then((res) => {
      let dataMap = res.data;

      // Set the initial data based on the selected year
      let selectedYear = yearSelector.value;
      let initialData = dataMap[selectedYear];

      var option = {
        color: [
          "#2c8df5",
          "#58c1d0",
          "#ff5c98",
          "#ea7e92",
          "#37c3d0",
          "#54d390",
          "#834F00",
          "#FFF100",
          "#8FC41F",
          "#ddd77f",
          "#dd8e7f",
          "#89f878",
          "#e6d885",
          "#dee6d5",
          "#67e788",
          "#957eef",
          "#d7e787",
          "#57e5e5",
          "#7799dd",
          "#7d65ee",
          "#6de566",
          "#5968d9",
          "#76887f",
          "#5def88",
          "#e769e8",
          "#569667",
          "#6ee6e6",
          "#7e8e5d",
          "#87f69f",
          "#6f7777",
          "#ff7e76",
          "#6555ee",
        ],

        title: [
          {
            top: "0%",
            left: "10%",
            text: "人口占比分布",

            textStyle: {
              color: "#fff",
              fontSize: "20",
              fontWeight: "normal",
            },
          },
        ],
        tooltip: {
          trigger: "item",
          formatter: "{a} <br/>{b}: {c} ({d}%)",
          position: function (p) {
            return [p[0] + 10, p[1] - 10];
          },
        },
        legend: {
          bottom: "0%",
          itemWidth: 10,
          itemHeight: 10,
          data: initialData.map((item) => item.name),
          textStyle: {
            color: "rgba(255,255,255,.5)",
            fontSize: "12",
          },
        },
        series: [
          {
            name: "人口占比分布",
            type: "pie",
            center: ["50%", "50%"],
            radius: ["30%", "60%"],
            //color: ['#065aab', '#066eab', '#0682ab', '#0696ab', '#06a0ab', '#06b4ab', '#06c8ab', '#06dcab', '#06f0ab'],
            label: { show: false },
            labelLine: { show: false },
            data: initialData,
          },
        ],
      };

      //筛选按钮
      document.getElementById("filter0").addEventListener("click", function () {
        let checkType = $("#yearSelector option:selected").val();
        var newData = dataMap[checkType];
        console.log(checkType, newData);
        myChart.setOption({
          series: [
            {
              data: newData,
            },
          ],
        });
      });

      myChart.setOption(option);
      window.addEventListener("resize", function () {
        myChart.resize();
      });
    });
  }
});
