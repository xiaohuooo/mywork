from django.apps import AppConfig


class ZiliaoConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "ziliao"
